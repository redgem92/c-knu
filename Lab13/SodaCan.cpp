#include "SodaCan.h"

CSodaCan::CSodaCan()
    :m_fHeight(0), m_fRadius(0)
{
    
}

CSodaCan::CSodaCan(double _fHeight, double _fRadius)
    :m_fHeight(_fHeight), m_fRadius(_fRadius)
{
    
}

CSodaCan::~CSodaCan()
{
    m_fHeight = 0;
    m_fRadius = 0;
}