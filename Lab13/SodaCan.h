#pragma once

const double PHI = 3.14160;

class CSodaCan
{
private:
    double m_fHeight;
    double m_fRadius;
public:
    double get_surface_area() const { return 2 * PHI * m_fRadius * m_fRadius + 2 * PHI * m_fRadius * m_fHeight; }
    double get_volume() const { return PHI * m_fRadius * m_fHeight; }
public:
    CSodaCan();
    CSodaCan(double _fHeight, double _fRadius);
    ~CSodaCan();
};
