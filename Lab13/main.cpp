#include "Header.h"

int main()
{
    CSodaCan can(2.5, 3);
    
    cout << "Surface : " << can.get_surface_area() << endl;
    cout << "Volume : " << can.get_volume() << endl;
    
    return 0;
}