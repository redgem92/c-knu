#include<iostream>
#include<cmath>

using namespace std;

int main()
{
    int num1, num2;
    cout << "Enter the number : ";
    cin >> num1 >> num2;
    
    cout << "The sum : " << num1 + num2 << endl;
    int ans = num1 - num2 > 0 ? num1 - num2 : num2 - num1;
    cout << "The difference : " << ans << endl; 
    cout << "The product : " << num1 * num2 << endl;
    cout << "The average : " << float(num1 + num2) / 2 << endl;
    
    return 0;
}