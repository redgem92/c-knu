#include<iostream>

const int FIRST_CLASS_STAMP_PRICE = 44;             //Price in pennies

using namespace std;

int main()
{
    cout << "Enter the number of dollars : ";
    
    int dollars;
    cin >> dollars;
    
    int first_class_stamps = 100 * dollars / FIRST_CLASS_STAMP_PRICE;
    int penny_stamps = 100 * dollars - first_class_stamps * FIRST_CLASS_STAMP_PRICE;
    
    cout << "The value = " << first_class_stamps << endl
         << "The penny stamps = " << penny_stamps << endl;
    
    return 0;
}