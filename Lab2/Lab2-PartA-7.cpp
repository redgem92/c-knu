#include<iostream>
#include<cmath>

#define PHI 3.14

using namespace std;

int main()
{
    int radius;
    cout << "Enter the radius : ";
    cin >> radius;
    
    cout << "Area in circle : " << PHI * radius * radius << endl;
    cout << "Volume and surface in sphere : " << (float)4 / 3 * PHI * pow(radius, 3) << ", " << (float)4 * PHI * pow(radius, 2)  << endl;
    
    return 0;
}