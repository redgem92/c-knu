#include<iostream>
#include<cmath>

using namespace std;

int main()
{
    cout << "The circumference of letter-size : " << (8.5 + 11) * 2  << " inches" << endl;
    cout << "The diagonal of letter-size : " << sqrt(pow(8.5, 2) + pow(11, 2)) << " inches" << endl;
    
    return 0;
}