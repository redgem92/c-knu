#include<iostream>

using namespace std;

int main()
{
    cout << "Enter value(0.25 = quarter, 1.25 = $1.25, 5 = 5$, etc.) : ";
    int value;
    cin >> value;
    
    cout << "Enter item price in pennies : ";
    int pennies;
    cin >> pennies;
    
    int rest = value * 100 - pennies;
    int coin = rest / 100;
    int quarter = (rest - coin * 100) / 25;
    
    cout << "Dollar coins : " << coin << endl
         << "Quarters : " << quarter << endl;
    
    
    return 0;
}