#include<iostream>
#include<cmath>

using namespace std;

int main()
{
    cout << "Enter the length for x distance : ";
    double x_distance;
    cin >> x_distance;
    
    cout << "Enter the length for y distance : ";
    double y_distance;
    cin >> y_distance;
    
    cout << "Enter the length of segment 1: ";
    double segment1_length;
    cin >> segment1_length;
    
    cout << "Enter the speed on the road in KMPH: ";
    double segment1_speed;
    cin >> segment1_speed;
    
    cout << "Enter the speed off the road in KMPH: ";
    double segment2_speed;
    cin >> segment2_speed;
    
    double time1 = segment1_length / segment1_speed;
    double time2 = sqrt(pow( x_distance, 2) + pow(y_distance - segment1_length, 2)) / segment2_speed;
    
    cout << "Time : " << time1 + time2 << endl;
    
    return 0;
}