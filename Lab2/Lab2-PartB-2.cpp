#include<iostream>

using namespace std;

int main()
{
    cout << "Enter the cost of a new car : ";
    int cost;
    cin >> cost;
    cout << "Enter the estimated miles driven per year : ";
    int mile;
    cin >> mile;
    cout << "Enter the estimated gas price : ";
    int gas;
    cin >> gas;
    cout << "Enter the estimated resale value after 5 years: ";
    int after5;
    cin >> after5;
    
    cout << "Total cost of owning the car for 5 yrs : "
         << cost + gas * (double)mile / 15000 - after5 << endl;
    
    return 0;
}