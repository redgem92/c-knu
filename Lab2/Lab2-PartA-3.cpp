#include<iostream>
#include<cmath>

using namespace std;

int main()
{
    int num;
    cout << "Enter the number : ";
    cin >> num;
    
    cout << "The Square of " << num << " : " << num * num << endl;
    cout << "The Cube of " << num << " : " << num * num * num << endl;
    cout << "The fourth of " << num << " : " << pow(num, 4) << endl;
    
    return 0;
}