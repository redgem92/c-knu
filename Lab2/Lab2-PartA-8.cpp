#include<iostream>
#include<cmath>

#define PHI 3.14

using namespace std;

int main()
{
    int row, col;
    cout << "Enter each row and col : ";
    cin >> row >> col;
    
    cout << "Area and perimeter : " << row * col << ", " << (row + col) * 2 << endl;
    cout << "Diagonal : " << sqrt(pow(row, 2) + pow(col, 2)) << endl;
    
    return 0;
}