#include<iostream>

using namespace std;

int main()
{
    int meter;
    cout << "Enter the distance for meter : ";
    cin >> meter;
    
    cout << meter << " m for miles : " << float(meter) / 1609 << endl;
    cout << meter << " m for feet : " << float(meter) / 0.3048 << endl;
    cout << meter << " m for yards : " << float(meter) / 0.9144 << endl;
    
    return 0;
}