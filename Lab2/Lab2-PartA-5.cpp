#include<iostream>

using namespace std;

int main()
{
    int num1, num2;
    cout << "Enter the number : ";
    cin >> num1 >> num2;
    
    int ans = num1 - num2 > 0 ? num1 - num2 : num2 - num1;
    cout << "The distance : " << ans << endl;
    ans = num1 - num2 > 0 ? num1 : num2;
    cout << "The maximum : " << ans << endl; 
    ans = num1 - num2 < 0 ? num1 : num2;
    cout << "The minimum : " << ans << endl;
    
    return 0;
}