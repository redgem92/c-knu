#include<iostream>

using namespace std;

int main()
{
    cout << "Enter the name : ";
    string name;
    cin >> name;
    cout << "Enter the salary : ";
    double salary;
    cin >> salary;
    cout << "How many hrs did " << name << " work in the past week? : ";
    int hrs;
    cin >> hrs;
    
    double final_salary;
    if(hrs > 40)
        final_salary = 1.5 * salary * hrs;
    else
        final_salary = salary * hrs;
    
    cout << name << "'s salary for last week is " << final_salary << endl;
    
    return 0;
}