#include<iostream>

using namespace std;

int main()
{
    int month, day;
    cin >> month >> day;
    
    string season;
    if(1 <= month && month <= 3)
    {   
        season = (month % 3 == 0 && day >= 21) ? "Spring" : "Winter";
    }
    else if(4 <= month && month <= 6)
    {
        season = (month % 3 == 0 && day >= 21) ? "Summer" : "Spring";
    }    
    else if(7 <= month && month <= 9)
    {
        season = (month % 3 == 0 && day >= 21) ? "Fall" : "Summer";
    }    
    else if(10 <= month && month <= 12)
    {
        season = (month % 3 == 0 && day >= 21) ? "Winter" : "Fall";
    }    
    
    cout << month << "/" << day << " is " << season << endl; ;

    return 0;
}