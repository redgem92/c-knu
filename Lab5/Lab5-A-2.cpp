#include<iostream>

using namespace std;

int main()
{
    float num1, num2;
    
    cout << "Enter two floating-pint numbers : ";
    cin >> num1 >> num2;
    
    if(-0.01 <= num1 - num2 && num1 - num2 <= 0.01)
        cout << "They are the same up to two decimal places." << endl;
    else
        cout << "They are different." << endl;
    
    return 0;
}