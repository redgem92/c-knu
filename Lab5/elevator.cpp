#include <iostream>

using namespace std;

int main()
{  
   int floor;
   cout << "Floor: ";
   cin >> floor;
   int actual_floor;
   
   if (floor > 13)
   {  
      	actual_floor = floor - 1;
        cout << "Method1: The elevator will travel to the actual floor "
      << actual_floor << endl;

   }
   else
   {
      	actual_floor = floor;
        cout << "Method1: The elevator will travel to the actual floor "
      << actual_floor << endl;

   }
//       Simplify the code above   
//     cout << "Method1: The elevator will travel to the actual floor "
//      << actual_floor << endl;

    actual_floor = floor;
    if (floor > 13)
    {
        actual_floor--;
    } // No else needed

    cout << "Method 2: The elevator will travel to the actual floor "
      << actual_floor << endl;
      
    actual_floor = floor;
    if (floor > 13)
        actual_floor--;
     // No brackets needed

    cout << "Method 3: The elevator will travel to the actual floor "
      << actual_floor << endl;
    
    
    // BUG!!!
    if (floor > 13); 
        actual_floor--;
    cout << "Method 4: The elevator will travel to the actual floor "
      << actual_floor << endl;
      
    actual_floor = floor > 13 ? floor - 1 : floor;
    cout << "Method 5: The elevator will travel to the actual floor "
      << actual_floor << endl;

    cout << "Mathod 6: Actual floor: " << (floor > 13 ? floor - 1 : floor) << endl;

     
   return 0;
}
