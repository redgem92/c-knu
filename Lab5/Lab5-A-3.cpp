#include<iostream>

using namespace std;

int main()
{

    cout << "Enter the initial balance of checking and savings : ";
    int checking_balance, savings_balance;
    cin >> checking_balance >> savings_balance;

    if(checking_balance < 0 || savings_balance < 0)
        cout << "Wrong initial balance." << endl;
    else
    {
        while(1)
        {
            cout << "Enter the request(1 : Deposit, 2 : Withdrawal, 3 : Transfer, etc : break) : ";
            int request;
            cin >> request;
            if(request == 1)
            {
                cout << "Deposit how much? : ";
                int money;
                cin >> money;
                savings_balance += money;
                cout << "Savings account : " << savings_balance << endl;
            }
            else if(request == 2)
            {
                cout << "Withdrawal how much? : ";
                int money;
                cin >> money;
                if(savings_balance - money < 0)
                    cout << "Not enough money" << endl;
                else
                {
                    savings_balance -= money;
                    cout << "Savings account : " << savings_balance << endl;
                }
            }
            else if(request == 3)
            {
                cout << "Transfer how much money from savings to checking : ";
                int money;
                cin >> money;
                if(savings_balance - money < 0)
                    cout << "Not enough money" << endl;
                else
                {
                    savings_balance -= money;
                    checking_balance += money;
                    cout << "Savings account : " << savings_balance << endl;
                    cout << "Checking account : " << checking_balance << endl;
                }
            }   
            else
              break;            
        }

    }
    
    return 0;
}