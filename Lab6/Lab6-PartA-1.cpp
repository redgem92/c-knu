#include<iostream>
#include<cmath>
#include<string>
#include<cstdlib>

using namespace std;

int main()
{
    int sum;
    
    for(int i = 2; i <= 100 ; ++i)
    {
        if(i % 2 == 0)
        sum += i;
    }
    
    cout << "Sum of all even numbers between 2 and 100 : " << sum << endl;
    
    sum = 0;
    for(int i = 1 ; i <= 100 ; ++i)
        sum += pow(i , 2);
        
    cout << "Sum of all squares between 1 and 100 : " << sum << endl;
    
    for(int i = 20 ; i <= 220 ; ++i)
        cout << i << "Power of 2 : " << pow(i, 2) << endl;
        
    cout << "Enter 2 values : ";
    int a, b; 
    cin >> a >> b;
    
    sum = 0;
    for(int i = a ; i <= b ; ++i)
    {
        if(i % 2 != 0)
            sum += i;
    }
    
    cout << "Sum of all even numbers between " << a << " and " << b << " : " << sum << endl;
    
    cout << "Enter the any number : ";
    string num;
    cin >> num;
    
    sum = 0;
    for(int i = 0 ; i < num.length() ; ++i)
    {
        if(atoi(num.substr(i, 1).c_str()) % 2 == 0)
            sum += atoi(num.substr(i, 1).c_str());
    }
    
    cout << "Sum of all even number " << num << " : " << sum << endl; 
    
    return 0;
}