#include<iostream>
#include<cmath>

using namespace std;

int main()
{
    cout << "Enter an interest rate : ";
    double rate;
    cin >> rate;
    
    cout << "Enter initial amount : ";
    double initial_balance;
    cin >> initial_balance;
    
    cout << "Enter target amount : ";
    double target;
    cin >> target;
    
    double balance = initial_balance;

    int year = 0;
    
    while(initial_balance * (1 + rate * 0.01f) * (pow(1 + rate * 0.01f, year) - 1) / (rate) < target)
        ++year;
    
    cout << "THE INVESTMENT DOUBLED AFTER " <<  year << " years" <<endl;

    return 0;
}