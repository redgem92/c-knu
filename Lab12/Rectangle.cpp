#include "Rectangle.h"

void Rectangle::SetVar()
{
    perimeter = width * 2 + length * 2;
    area = width * length;
}

void Rectangle::set_length(double l)
{
    length = l;
    SetVar();
}

void Rectangle::set_width(double l)
{
    width = l;
    SetVar();
}

void Rectangle::resize(double factor)
{
    width *= factor;
    length *= factor;
    SetVar();
}
