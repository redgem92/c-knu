#pragma once

class Rectangle
{
private:
    double width;
    double length;
    double perimeter;
    double area;
private:
    void SetVar();
public:
    double get_perimeter()const { return perimeter; }
    double get_area() const { return area; }
    double get_width() const { return width; }
    double get_length() const { return length; }
    void set_length(double l);
    void set_width(double l);
    void resize(double factor);
public:
    Rectangle()
        :width(1), length(2), perimeter(width * 2 + length * 2), area(width * length)
    {
        
    }
};