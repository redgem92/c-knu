#include<iostream>

using namespace std;

class Counter
{
private:
    int m_iCount;
public:
    void count()
    {
        ++m_iCount;
    }
    int GetCount() const{return m_iCount;}
public:
    Counter()
        :m_iCount(0)
    {
        
    }
};


int main()
{
    Counter cnt;
    
    cnt.count();
    cnt.count();
    cnt.count();
    
    cout << cnt.GetCount() << endl;
    
    return 0;
}