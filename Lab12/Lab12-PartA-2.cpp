#include "Header.h"

int main()
{
    Rectangle rec;
    
    cout << rec.get_width() << " * " << rec.get_length() << " = " << rec.get_area() << endl;
    
    return 0;
}