#include<iostream>
#include<cstdlib>
#include<ctime>
#include<vector>

using namespace std;

const int CAPACITY = 5;
const int TIME = 12;

void release(int* _pComp, int* _pUser, int** _pUserList);
void Review(int** _pUserList, int* _pComp, int* _pCount);
bool Guess(int* _pComp, int* _pUser, int** _pUserList, int* _pCount);
bool isInvalid(int* ary);
void print_array(int* _pAry);
int correct(int* _pComp, int* _pUser);
bool isOverlap(int* ary, int i, int value);
int right_pos(int* _pComp, int* _pUser);

int main()
{
    int*        pComp;
    int*        pUser;
    int**       pUserList;
    int         iCount;
    
    pComp = new int[CAPACITY];
    pUser = new int[CAPACITY];
    pUserList = new int*[TIME];
    
    for(int i = 0 ; i < TIME ; ++i)
       pUserList[i] = new int[CAPACITY];
    
    srand(time(NULL));
    for(int i = 0 ; i < CAPACITY ; ++i)
        *(pComp + i) = rand() % 10;  
    
    iCount = 0;
    
    while(true)
    {
        cout << "Input the order((G)uess, (R)eview Guess, (N)ew Game, (Q)uit Game : ";
        cin.clear();
        char input;
        cin >> input;
        switch(input)
        {
            case 'g':
            case 'G':
                if(!Guess(pComp, pUser, pUserList, &iCount))
                    return 0;
                break;
            case 'r':
            case 'R':
                Review(pUserList, pComp, &iCount);
                break;
            case 'n':
            case 'N':
                cout << "Start new game" << endl;
                release(pComp, pUser, pUserList);
                
                pComp = new int[CAPACITY];
                pUser = new int[CAPACITY];
                pUserList = new int*[TIME];
                
                for(int i = 0 ; i < TIME ; ++i)
                   pUserList[i] = new int[CAPACITY];
                
                srand(time(NULL));
                for(int i = 0 ; i < CAPACITY ; ++i)
                    *(pComp + i) = rand() % 10;  
                
                iCount = 0;
                
                break;
            case 'q':
            case 'Q':
                cout << "Bye!" << endl;
                release(pComp, pUser, pUserList);
                return 0;
            default:
                cout << "Invalid input" << endl;
        }
    }
    
    return 0;
}

/**
 * Return how many numbers are in right postion
 * @param Secret code array
 * @param Guess code array
 * @return Number of right position
 * */
int right_pos(int* _pComp, int* _pUser)
{
    int iCount = 0;
    
    for(int i = 0 ; i < CAPACITY ; ++i)
    {
        if(*(_pComp + i) == *(_pUser + i))
            ++iCount;
    }
    
    return iCount;
}

/**
 * Check whether the random number is overlapped or not
 * @param Array to input
 * @param Index of checking array
 * @param Value to input
 * @return true if overlapped, else false
 * */
bool isOverlap(int* ary, int i, int value)
{
    for(int j = 0 ; j < i ; ++j)
    {
        if(*(ary + j) == value)
            return true;
    }
    return false;
}

/**
 * Return how many numbers are included in secret number
 * @param Array of secret code
 * @param Array of guess code
 * @return Number of inclusion
 * */
int correct(int* _pComp, int* _pUser)
{
    int iCount = 0;
    
    for(int i = 0 ; i < CAPACITY ; ++i)
    {
        if(!isOverlap(_pUser, i, _pUser[i]))                                                            //Not counting the number overlapped
        {
            for(int j = 0 ; j < CAPACITY ; ++j)
            {
                if(*(_pUser + i) == *(_pComp + j) && !isOverlap(_pComp, j, _pComp[j]))                  //So is it
                    ++iCount;
            }
        }
    }
    
    return iCount;
}

/**
 * Print array
 * @param Array to print
 * */
void print_array(int* _pAry)
{
    for(int i = 0 ; i < CAPACITY ; ++i)
    {
        cout << *(_pAry + i);
        if(i != CAPACITY - 1)
            cout << ", ";
    } 
    cout << endl;        
}

/**
 * Check whether the number in array is 0~9 or not
 * @param Array to check
 * @return true if invalid, else false
 * */
bool isInvalid(int* ary)
{
    for(int i = 0 ; i < CAPACITY ; ++i)
    {
        if(*(ary + i) >= 10 || *(ary + i) < 0)
            return true;
    }
    return false;
}

/**
 * Matching the code secret one and guess one
 * @param Array of secret code
 * @param Array of guess code
 * @param Array of guess cod list
 * @param How many times tried
 * @return true if guess code is invalid or find secret code(review if find secret code)
 *          fail then return false
 * */
bool Guess(int* _pComp, int* _pUser, int** _pUserList, int* _pCount)
{
    cin.clear();
    cout << "Enter a guess for each of 5 numbers : ";
    for(int i = 0 ; i < CAPACITY ; ++i)
    {
        cin >> *(_pUser + i);
        *(*(_pUserList + *_pCount) + i) = *(_pUser + i);
    }
    
    if(isInvalid(_pUser))
    {
        cout << "Guess number should be 0~9" << endl;
        for(int i = 0 ; i < CAPACITY ; ++i)
        {
            *(*(_pUserList + *_pCount) + i) = 0;
        }
        return true;
    }
    
    cout << "User : ";
    print_array(_pUser);
    
    ++*_pCount;
    if(right_pos(_pComp, _pUser) == CAPACITY)
    {
        cout << "Congulation!! You are the winner!!" << endl;
        cout << "Your answer : " ;
        print_array(_pUser);
        print_array(_pComp);
        cout << "How many times you tried : " << *_pCount << endl;
        return true;
    }
    cout << "Correct : " << correct(_pComp, _pUser) << endl;
    cout << "Right position : " << right_pos(_pComp, _pUser) << endl;
    
    if(*_pCount >= TIME)
    {
        cout << "You lose" << endl;
        cout << "Secret numbers : ";
        print_array(_pComp);
        return false;
    }    
    
    return true;
}

/**
 * Review my guess code
 * @param List of array of guess code
 * @param Array of secret code
 * @param Number of lists I have
 * */
void Review(int** _pUserList, int* _pComp, int* _pCount)
{
    cout << "Secret Code is ";
    print_array(_pComp);
    for(int i = 0 ; i < *_pCount; ++i)
    {
        cout << "Guess " << i + 1 << ":";
        for(int j = 0 ; j < CAPACITY ;++j)
        {
            cout << _pUserList[i][j];
            if(j != CAPACITY - 1)
                cout << ", ";
        }
        cout << ": " << correct(_pComp, _pUserList[i]) << " numbers correct, " << right_pos(_pComp, _pUserList[i]) << " in right position." << endl;
    }
}

/**
 * Deallocate whole array I allocated
 * @param Array of secret code
 * @param Array of guess code
 * @param List of array of guess code
 * */
void release(int* _pComp, int* _pUser, int** _pUserList)
{
    delete[] _pComp;
    _pComp = NULL;
    delete[] _pUser;
    _pUser = NULL;
    for(int i = 0 ; i < TIME ; ++i)
    {
        delete[] _pUserList[i];
        _pUserList[i] = NULL;
    }
    delete[] _pUserList;
}
