#include <iostream>
#include <string>

using namespace std;

/**
   Turns a digit into its English name.
   @param digit an integer between 1 and 9
   @return the name of digit ("one" ... "nine")
*/
string digit_name(int digit)
{  
    if(1 <= digit && digit <= 9)
    {
        switch(digit)
        {
            case 1:
                return "one";
                break;
            case 2:
                return "two";
                break;
            case 3:
                return "three";
                break;
            case 4:
                return "four";
                break;
            case 5:
                return "five";
                break;
            case 6:
                return "six";
                break;
            case 7:
                return "seven";
                break;
            case 8:
                return "eight";
                break;
            case 9:
                return "nine";
                break;
            default:
                cout << "Number is not between 1 and 9" << endl;
        }

    }
    return NULL;
}

/**
   Turns a number between 10 and 19 into its English name.
   @param number an integer between 10 and 19
   @return the name of the given number ("ten" ... "nineteen")
*/
string teen_name(int number)
{  
    if(10 <= number && number <= 19)
    {
        switch(number)
        {
            case 10:
                return "ten";
                break;
            case 11:
                return "eleven";
                break;
            case 12:
                return "twelve";
                break;
            case 13:
                return "thirteen";
                break;
            case 14:
                return "fourteen";
                break;
            case 15:
                return "fifteen";
                break;
            case 16:
                return "sixteen";
                break;
            case 17:
                return "seventeen";
                break;
            case 18:
                return "eighteen";
                break;
            case 19:
                return "nineteen";
                break;
            default:
                cout << "Number is not between 10 and 19" << endl;
        }

    }
    return NULL;
}

/**
   Gives the name of the tens part of a number between 20 and 99.
   @param number an integer between 20 and 99
   @return the name of the tens part of the number ("twenty" ... "ninety")
*/
string tens_name(int number)
{  
    if(20 <= number && number <= 29)
    {
        return "twenty";
    }
    else if(30 <= number && number <= 39)
    {
        return "thirty";
    }
    else if(40 <= number && number <= 49)
    {
        return "fourty";
    }
    else if(50 <= number && number <= 59)
    {
        return "fifty";
    }
    else if(60 <= number && number <= 69)
    {
        return "sixty";
    }
    else if(70 <= number && number <= 79)
    {
        return "seventy";
    }
    else if(80 <= number && number <= 89)
    {
        return "eighty";
    }
    else if(90 <= number && number <= 99)
    {
        return "nienty";
    }
    else
    {
        cout << "The number is not between 20 and 99" << endl;
        return NULL;
    }
}

/**
   Turns a number into its English name.
   @param number a positive integer < 1,000
   @return the name of the number (e.g. "two hundred seventy four")
*/
string int_name(int number)
{
   int part = number; // The part that still needs to be converted 
   string name; // The return value

   if (part >= 100)
   {  
      name = digit_name(part / 100) + " hundred";
      part = part % 100;
   }

   if (part >= 20)
   {  
      name = name + " " + tens_name(part);
      part = part % 10;
   }   
   else if (part >= 10)
   {  
      name = name + " " + teen_name(part);
      part = 0;
   }

   if (part > 0)
   {
      name = name + " " + digit_name(part);
   }

   return name;
}

int main()
{  
   cout << "Please enter a positive integer: ";
   int input;
   cin >> input;
   cout << int_name(input) << endl;
   return 0;
}
