#include<iostream>
#include<algorithm>
#include<cmath>
#include<cstdlib>

using namespace std;

int first_digit(int n)
{
    for(int i = 0 ; i < 5 ; i++)
    {
        if(n / pow(10, i) < 10)
            return n / pow(10, i);
    }
    return 0;
}

int last_digit(int n)
{
    return n % 10;
}

int digit(int n)
{
    int count = 1;
    for(int i = 0 ; i < 5 ; i++)
    {
        if(n / pow(10, i) >= 10)
            ++count;
    }
    return count;
}

int main()
{
    cout << "first_digit : " << first_digit(23567) << endl;
    cout << "last_digit : " << last_digit(23567) << endl;
    cout << "last_digit : " << digit(23567) << endl;
    
    return 0;
}