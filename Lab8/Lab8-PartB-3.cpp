#include<iostream>
#include<algorithm>

using namespace std;

int count_words(string str)
{  
    if(str.length() == 0)
        return 0;
        
    int count = 1;
    bool isVar = false;
    for(int i = 0 ; i < str.length() ; ++i)
    {
        if(str.at(i) == ' ')
        {
            if(isVar)
            {
                count++;
                isVar = false;
            }
        }
        else
        {
            isVar = true;
        }
    }
    
    return count;
}

int main()
{
    string str = "hOI!!! iM teMMIE!!!";
    
    cout << count_words(str) << endl;
    
    return 0;
}