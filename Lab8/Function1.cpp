#include<iostream>

using namespace std;

/**
 * Computes a volume of cube
 * @param side_length of the side of a cube
 * @return the volume
 * */

double cube_volume(double side)
{
    return side * side * side;
}

double cube_volumeB(double side)
{
    if(side >= 0)
        return side * side * side;
}

int main()
{
    for(int i = 1 ; i <= 10 ; ++i)
    {
        double result = cube_volume(i);
        cout << "A cube with side length " << i << " is = " << result << endl;
    }
    
    double result2 = cube_volumeB(-1);
    cout << "A cube with side length -1 is = " << result2 << endl;
    return 0;
}