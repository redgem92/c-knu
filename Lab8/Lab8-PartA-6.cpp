#include<iostream>
#include<algorithm>

using namespace std;

string middle(string str)
{
    if(str.length() % 2 == 0)
        return str.substr(str.length() / 2, 2);
    return str.substr(str.length() / 2, 1); 
}

int main()
{
    string str = "Center";
    cout << "Middle word(s) of " << str << " is " << middle(str) << endl;
    str = "Brother";
    cout << "Middle word(s) of " << str << " is " << middle(str) << endl;
    str = "Sister";
    cout << "Middle word(s) of " << str << " is " << middle(str) << endl;
    
    return 0;
}