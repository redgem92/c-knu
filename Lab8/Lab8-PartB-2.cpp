#include<iostream>
#include<algorithm>

using namespace std;

int count_vowels(string str)
{  
    int count = 0;
    
    for(int i = 0 ;i < str.length() ; ++i)
    {
        if(str.at(i) == 'a' || str.at(i) == 'e' || 
            str.at(i) == 'o' || str.at(i) == 'u' || 
            str.at(i) == 'i' || str.at(i) == 'A' || 
            str.at(i) == 'E' || str.at(i) == 'O' || 
            str.at(i) == 'U' || str.at(i) == 'I')
            count++;
    }
    
    return count;
}

int main()
{
    string str = "hOI!!! iM teMMIE!!!";
    
    cout << count_vowels(str) << endl;
    
    return 0;
}