#include<iostream>
#include<algorithm>

using namespace std;

double balance(double initial_bal, double rate)
{
    cout << "How many years? : ";
    int yrs; cin >> yrs;
    
    double res = initial_bal;
    for(int i = 0 ; i < yrs ; ++i)
        res += res * rate * 0.01f;
    return res;
}

int main()
{
    cout << "Enter initial balance and rate(%) : ";
    double initBal, rate; cin >> initBal >> rate;
    
    cout << "Balance : " << balance(initBal, rate) << endl;
    
    return 0;
}