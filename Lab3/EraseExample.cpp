#include<iostream>
#include<string>

using namespace std;

int main()
{
    string text = "hello world, this is a test";
    
    string fragment1 = text.substr(6, 5);       //Start at pos 6 and take 5 characters
    string fragment2 = text.substr(3);          //Start at 3 and go all way to the endif
    //string fragment3 = text.substr(28, 2);      //Start at 28 and take 2 chars
    string fragment4 = text.substr(2, 30);      //Start at 2 and take 30 chars
    string fragment5 = text;
    fragment5.erase(0, 5);
    string fragment6 = text.replace(18, 2, "was");  //replace 'is' with 'was'
    
    cout << "Original : " << text << endl;
    cout << "Line1 : " << fragment1 << endl;
    cout << "Line2 : " << fragment2 << endl;
    //cout << "Line3 : " << fragment3 << endl;
    cout << "Line4 : " << fragment4 << endl;
    cout << "Line5 : " << fragment5 << endl;
    cout << "Line6 : " << fragment6 << endl;
    
    return 0;
}