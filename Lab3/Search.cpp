#include<iostream>
#include<string>

#define ASSIGN

using namespace std;

int main()
{
#ifndef ASSIGN
    string text = "Hello world, this is a test";
    string textin;
    cout << "Enter a string" << endl;
    getline(cin, textin);
    
    //It can figure out the string in 'find' function by string::npos
    if(textin.find(' ') != string::npos)
        cout << "Contains at least 1 space!" << endl;
    else
        cout << "Does not contain any space" << endl;
#endif
        
#ifdef ASSIGN
    string password = "1q2w3e4r!";
    
    cout << "Enter the password" << endl;
    string input; getline(cin, input);

    if(password == input)
        cout << "Welcome to C++ wolrd!" << endl;
    else
        cout << "Wrong password!" << endl;
#endif
        
    return 0;
}