#include<iostream>
#include<string>
#include<cmath>
#include<iomanip>

using namespace std;

const double PHI = 3.14;                
const double a = 17.27;               //Local var. to Global var.
const double b = 237.7;               //Local var. to Global var.

//Function for calculating dpt
double Func(double T, double RH);

int main()
{
    cout << "Operator : ";
    string op; getline(cin, op);
    double Num1, Num2, Ans;
    if(op == "+")
    {
        //Add Num1 to Num2
        cout << "Num1 : ";
        cin >> Num1;
        cout << "Num2 : ";
        cin >> Num2;
        
        Ans = Num1 + Num2;
        cout << Num1 << " " << op << " " << Num2 << " = " << fixed << setprecision(2) << Ans << endl;
    }
    else if(op == "-")
    {
        //Subtract Num2 from Num1
        cout << "Num1 : ";
        cin >> Num1;
        cout << "Num2 : ";
        cin >> Num2;
        
        Ans = Num1 - Num2;
        if(Num2 < 0 )
        {
            Num2 *= -1;
            op = "+";
        }
        cout << Num1 << " " << op << " " << Num2 << " = " << fixed << setprecision(2) << Ans << endl;
    }
    else if(op == "*")
    {
        //Multiply Num2 from Num1
        cout << "Num1 : ";
        cin >> Num1;
        cout << "Num2 : ";
        cin >> Num2;
        
        Ans = Num1 * Num2;
        cout << Num1 << " " << op << " " << Num2 << " = " << fixed << setprecision(2) << Ans << endl;
    }
    else if(op == "/")
    {
        //Divide A by B
        cout << "Num1 : ";
        cin >> Num1;
        cout << "Num2 : ";
        cin >> Num2;
        
        Ans = Num1 / Num2;
        cout << Num1 << " " << op << " " << Num2 << " = " << fixed << setprecision(2) << Ans << endl;
    }
    else if(op == "pow")
    {
        //A to the power of B 
        cout << "Num1 : ";
        cin >> Num1;
        cout << "Num2 : ";
        cin >> Num2;
        
        Ans = pow(Num1, Num2);
        cout << Num1 << " to the power of " << Num2 << " = " << fixed << setprecision(2) << Ans << endl;
    }
    else if(op == "root")
    {
        //the Ath root of B
        cout << "Num1 : ";
        cin >> Num1;
        cout << "Num2 : ";
        cin >> Num2;
        
        Ans = pow(Num1, (double)1 / Num2);
    
        cout << Num1 << "-th root of " << Num2 << " = " << fixed << setprecision(2) << Ans << endl;
    }
    else if(op == "sine")
    {
        //sine of A
        cout << "Num1 : ";
        cin >> Num1;
        
        Ans = sin(Num1);
        cout << "Sine of " <<  Num1 << " = " << fixed << setprecision(2) << Ans << endl;
    }
    else if(op == "log")
    {
        //log of A 
        cout << "Num1 : ";
        cin >> Num1;
        
        Ans = log10(Num1);
        cout << "Log10 of " << Num1 << " = " << fixed << setprecision(2) << Ans << endl;
    }
    else if(op == "help")
    {
        //Print out help for the program
        cout << "1. +\t\tA + B\t\t\t[Add A to B]" << endl;                                                        
        cout << "2. -\t\tA - B\t\t\t[Subtract B from A]" << endl;
        cout << "3. *\t\tA * B\t\t\t[Multiply A and B]" << endl;
        cout << "4. /\t\tA / B\t\t\t[Divide A by B]" << endl;
        cout << "5. pow\t\tA B\t\t\t[A to the power of B]" << endl;
        cout << "6. root\t\t\t\t\t[the Ath root of B]" << endl;
        cout << "7. sine\t\tSin(A)\t\t\t[sine of A]" << endl;
        cout << "8. log\t\tLog 10 (A)\t\t[log of A]" << endl;
        cout << "9. help\t\t\t\t\t[Print out help for the program]" << endl;
        cout << "10. cyl\t\t\t\t\t[Volume of a cylinder radius r and height h]" << endl;
        cout << "11. int\t\tP * R * 10\t\t[Simple interest for 10yrs, principle P and % rate R]" << endl;
        cout << "12. dpt\t\tT R\t\t\t[Dew point temperature using temperature T and" << endl;
        cout << "\t\t\t\t\trelative humidity R as per formula below]" << endl;
    }
    else if(op == "cyl")
    {
        //Volume of a cylinder radius r and height h
        cout << "Radius : ";
        cin >> Num1;
        cout << "Height : ";
        cin >> Num2;
        Ans = PHI * Num1 * Num1 * Num2;
        cout << "Volume of a cylinder radius " << Num1 << " and height " << Num2 << " = " << fixed << setprecision(2) << Ans << endl;
    }
    else if(op == "int")
    {
        //Simple interest for 10yrs, principle P and % rate R
        cout << "Principle : ";
        cin >> Num1;
        cout << "Percents of Rate : ";
        cin >> Num2;
        Ans = 10 * Num1 * Num2;    
        cout << "Simple interest for 10yrs, principle " << Num1 << " and percentage rate R " << Num2 << " = " << fixed << setprecision(2) << Ans << endl;
    }
    else if(op == "dpt")
    {
        //Dew point temperature using temperature T and
        //relative humidity R as per formula below
        cout << "Temperature : ";
        cin >> Num1;
        cout << "Humidity : ";
        cin >> Num2;
        
        Ans = b * Func(Num1, Num2) / (a - Func(Num1, Num2));
        cout << "Dew point temperature using temperature " << Num1 << " and relative humidity " << Num2 << " as per formula below = " << fixed << setprecision(2) << Ans << endl;
    }
    else
    {
        //When input the invalid operator
        cout << "Invalid operator. Input the 'help' to check operators out." << endl;
    }
        
    return 0;
}

double Func(double T, double RH)
{
    return a * T / (b + T) + log(RH);
}