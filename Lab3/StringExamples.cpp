#include<iostream>
#include<string>

using namespace std;

int main()
{
    //Initializing and adding strings together
    string result;
    string s1 = "Hello";
    string s2 = "Korea";
    result = s1 + s2;
    
    cout << result << endl;
    
    //Appending
    string result1;
    string s3 = "Hello";
    string s4 = "World";
    result1 = s3;
    result1 += ' ';
    result1 += s4;
    
    cout << result1 << endl;
    
    //Concatonating multiple items
    string firstname, lastname, fullname;
    cout << "First name : ";
    cin >> firstname;
    cout << "Last name : ";
    cin >> lastname;
    
    fullname = lastname + "," + firstname;
    cout << "Full name : " << fullname << endl;
    
    //Comparing strings
    string stringcomp1 = "hello";
    string stringcomp2 = "hello";
    string stringcomp3 = "world";
    
    if(stringcomp1 == stringcomp2)
        cout << "Same strings" << endl;
    else
        cout << "Not the same" << endl;
        
    if(stringcomp1 == stringcomp3)
        cout << "Same strings" << endl;
    else
        cout << "Not the same" << endl;
    
    return 0;
}