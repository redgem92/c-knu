#include<iostream>

using namespace std;

int main()
{
    int count = 0;
    
    cout << "Enter a word : ";
    string word;
    getline(cin, word);                     //Rather than cin, since it counts spaces.
    
    for(int i = 0 ; i < word.length() ; ++i)
    {
        string letter = word.substr(i, 1);
        if(letter == "a" || letter == "e" ||
            letter == "i" || letter == "o" ||
            letter == "u" || letter == "y" ||
            letter == "a" - 32|| letter == "e" - 32 ||
            letter == "i" - 32|| letter == "o" - 32 ||
            letter == "u" - 32|| letter == "y" - 32)
        {
            ++count;            
        }
    }
    
    cout << "There were " << count << " letters of vowel" << endl;
    
    return 0;
}