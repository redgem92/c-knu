#include<iostream>

using namespace std;

int main()
{
    cout << "Enter a palindrome : ";
    string str;
    getline(cin, str);
    
    if(str.length() == 0)
    {
        cout << "This is a invalid palindrome" << endl; 
        return 0;
    }
    
    cout << "Do you want to ignore case? : ";
    char choice;
    cin >> choice;
    if(choice == 'y' || choice == 'Y')                                      //For ingoring case
    {
        
        for(int i = 0 ; i < str.length() / 2 ; ++i)
        {
            if(str.at(i) != str.at(str.length() - 1 - i) &&                 //If it is not exactly same
                str.at(i) != str.at(str.length() - 1 - i) - 32 &&           //If it is not just same case(Lower and Upper)
                str.at(i)  != str.at(str.length() - 1 - i) + 32)            //If it is not just same case(Upper and Lower)
            {
                cout << "This is a invalid palindrome" << endl; 
                return 0;
            }
        }
        
        cout << "This is a valid palindrome" << endl;    
    }
    else if(choice == 'n' || choice == 'N')                                 //For doing care about case
    {  
        for(int i = 0 ; i < str.length() / 2 ; ++i)
        {
            if(str.at(i) != str.at(str.length() - 1 - i))
            {
                cout << "This is a invalid palindrome" << endl; 
                return 0;
            }
        }
        
        cout << "This is a valid palindrome" << endl;           
    }
    else
    {
        cout << "Invalid answer" << endl;
        return 0;
    }
        
    return 0;
}