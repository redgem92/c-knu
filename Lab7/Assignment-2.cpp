#include<iostream>

using namespace std;

int main()
{
    cout << "Enter a palindrome : ";
    string str;
    getline(cin, str);
    
    if(str.length() == 0)                                       //No length. It cant be a word...
    {
        cout << "This is a invalid palindrome" << endl; 
        return 0;
    }
    
    for(int i = 0 ; i < str.length() / 2 ; ++i)                 //No need to checking center so I just checking all except for mid.
    {
        if(str.at(i) != str.at(str.length() - 1 - i))           //If one thing is different, it shall be invalid
        {
            cout << "This is a invalid palindrome" << endl; 
            return 0;
        }
    }
    
    cout << "This is a valid palindrome" << endl;
        
    return 0;
}