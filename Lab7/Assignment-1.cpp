#include<iostream>
#include<cstdlib>

using namespace std;

int main()
{
    cout << "Enter length of Palindrome : ";
    int length;
    cin >> length;
    
    if(length == 0)                                     //There is no 0-length password
    {
        cout << "This is invalid password" << endl;
        return 0;
    }
    
    char* chPal = new char[length];
    
    srand(time(0));                                     //Function for random numbers
    for(int i = 0 ; i < length / 2 ; ++i)
    {  
        int iTmp = rand() % 94 +32;                     //Random integer number
        chPal[i] = iTmp;                                //Converting to char
        chPal[length - 1 - i] = chPal[i];               //Put in opposite array for palindrome
    }
    
    if(length % 2 != 0)                                 //If length is odd, it shall put random char in ary[length / 2]
    {
        int iTmp = rand() % 94 +32;
        chPal[length / 2] = iTmp;
    }
    
    cout << chPal << endl;                              //Print result
    delete chPal;                                       //Delete ary what I allocated.
        
    return 0;
}