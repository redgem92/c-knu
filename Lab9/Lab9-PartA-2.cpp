#include<iostream>
#include<cstdlib>
#include<ctime>
#include<algorithm>

using namespace std;

const int CAPACITY = 10;

struct Greater
{
    bool operator()(int a, int b) const
    {
        return a > b;
    }
};

bool isOverlap(int ary[], int i, int value)
{
    for(int j = 0 ; j < i ; ++j)
    {
        if(ary[j] == value)
            return true;
    }
    return false;
}

int main()
{
    int ary[CAPACITY];
    
    srand(time(NULL));
    int i = 0;
    while(i < CAPACITY)
    {
        ary[i] = rand() % 100;
        if(isOverlap(ary, i, ary[i]))
            continue;
        ++i;
    }
    
    cout << "Primitive array" << endl;
    for(int i = 0 ; i < CAPACITY ; ++i)
        cout << ary[i] << ' ';
    cout << endl;
    
    //Swap
    int iTmp = ary[0];
    ary[0] = ary[CAPACITY - 1];
    ary[CAPACITY - 1] = iTmp;
    
    cout << "Swapped array" << endl;
    for(int i = 0 ; i < CAPACITY ; ++i)
        cout << ary[i] << ' ';
    cout << endl;
    
    //Shift
    iTmp = ary[CAPACITY - 1];
    for(int i = CAPACITY - 2 ; i >= 0 ; --i)
    {
        ary[i + 1] = ary[i];
    }
    ary[0] = iTmp;
    
    cout << "Shifted array" << endl;
    for(int i = 0 ; i < CAPACITY ; ++i)
        cout << ary[i] << ' ';
    cout << endl;
    
    //Replace 0
    cout << "Replaced array" << endl;
    for(int i = 0 ; i < CAPACITY ; ++i)
    {
        if(i % 2 == 0)
            ary[i] = 0;
        cout << ary[i] << ' ';
    }
    cout << endl;
    
    //Replace 2
    sort(ary + 1, ary + CAPACITY - 1, Greater());
    
    cout << "Replaced 2nd array" << endl;
    for(int i = 0 ; i < CAPACITY ; ++i)
        cout << ary[i] << ' ';
    cout << endl;
    
    //Remove
    cout << "Removed array" << endl;
    ary[CAPACITY / 2] = 0;
    if(CAPACITY % 2 == 0)
    {
        ary[CAPACITY / 2 - 1] = 0;
    }
    for(int i = 0 ; i < CAPACITY ; ++i)
        cout << ary[i] << ' ';
    cout << endl;
    
    //Remove
    cout << "Removed 2nd array" << endl;
    for(int i = 0 ; i < CAPACITY ; ++i)
    {
        if(ary[i] % 2 == 0)
            ary[i] = 0;
        cout << ary[i] << ' ';
    }
    cout << endl;
    
    //if sorted in increasing order
    //if contained two adjacent duplicate values
    //if contained duplicate values
    return 0;
}