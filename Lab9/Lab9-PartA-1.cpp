#include<iostream>
#include<cstdlib>
#include<ctime>
#include<algorithm>

using namespace std;

struct Greater
{
    bool operator()(int a, int b) const
    {
        return a > b;
    }
};

bool isOverlap(int ary[], int i, int value)
{
    for(int j = 0 ; j < i ; ++j)
    {
        if(ary[j] == value)
            return true;
    }
    return false;
}

int main()
{
    int ary[10];
    
    srand(time(NULL));
    int i = 0;
    while(i < 10)
    {
        ary[i] = rand() % 100;
        if(isOverlap(ary, i, ary[i]))
            continue;
        ++i;
    }
    
    sort(ary, ary + 10);
    
    cout << "Every element at an even index" << endl;
    for(int i = 0 ; i < 10 ; i++)
    {
        if(i % 2 == 0)
            cout << ary[i] << " ";
    }
    cout << endl;
    
    cout << "Every even element" << endl;
    for(int i = 0 ; i < 10 ; i++)
    {
        if(ary[i] % 2 == 0)
            cout << ary[i] << " ";
    }
    cout << endl;
    
    cout << "All elements in reverse order" << endl;
    sort(ary,ary + 10, Greater());
    for(int i = 0 ; i < 10 ; i++)
    {
        cout << ary[i] << " ";
    }
    cout << endl;
    
    cout << "Only the first and last element" << endl;
    cout << ary[0] << ", " << ary[9] << endl;

    return 0;
}