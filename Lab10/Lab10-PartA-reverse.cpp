#include<iostream>

using namespace std;

const int CAPACITY = 5;

void swap(double* a, double* b)
{
    double tmp = *a;
    *a = *b;
    *b = tmp;
}

void reverse(double* a, int size)
{
    for(int i = 0 ; i < size / 2 ; ++i)
        swap(a + i, a + size - i - 1);
}

int main()
{
    double a[CAPACITY] = { 1.2323,4,5.232,7.2,2.34};
    
    for(int i = 0 ; i < CAPACITY ; ++i)
        cout << *(a + i) << " ";
    cout << endl;
    
    reverse(a, CAPACITY);
    
    for(int i = 0 ; i < CAPACITY ; ++i)
        cout << *(a + i) << " ";
    cout << endl;
    
    return 0;
}