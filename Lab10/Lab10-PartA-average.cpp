#include<iostream>

using namespace std;

const int CAPACITY = 5;

double average(double* a, int size)
{
    double result = 0;
    for(int i = 0 ; i < CAPACITY ; ++i)
    {
        result += *(a + i);
    }
    return result / CAPACITY;
}

int main()
{
    double a[CAPACITY] = { 1.2323,4,5.232,7.2,2.34};
    cout << average(a, CAPACITY) << endl;
    
    return 0;
}