#include<iostream>

using namespace std;

const int CAPACITY = 5;

double* maximum(double* a, int size)
{
    double max = 0;
    int i = 0;
    while(i < size || i < CAPACITY)
    {
        if(*(a+i) > max)
            max = *(a+i);
        ++i;
    }
    return &max;
}

int main()
{
    double a[CAPACITY] = { 1.2323,4,5.232,7.2,2.34};
    cout << *maximum(a, CAPACITY) << endl;
    
    return 0;
}