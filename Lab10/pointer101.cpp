#include<iostream>

using namespace std;

int main()
{
    int iVar = 10;
    int iAnotherVar = 20;
    int* pVar = &iVar;
    
    cout << "iVar : " << iVar << endl;
    cout << "iAnotherVar : " << iAnotherVar << endl;
    cout << "&iAnotherVar : " << &iAnotherVar << endl;
    cout << "&iVar : " << &iVar << endl;
    
    cout << "pVar points to = " << *pVar << endl;
    cout << "Which means pVar = " << pVar << endl;
    
    pVar = &iAnotherVar;
    
    cout << "pVar points to = " << *pVar << endl;
    cout << "Which means pVar = " << pVar << endl;

    return 0;
}